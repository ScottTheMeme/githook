FROM codedgellc/alpine-elixir-phoenix:1.11 as builder

WORKDIR /githook


ARG VERSION

ENV MIX_ENV prod

RUN apk add --update --no-cache bash git openssh openssl
    # Phoenix needs these

RUN mix local.hex --force, mix local.rebar --force

COPY mix.exs mix.lock VERSION ./
COPY config config
COPY assets assets
COPY priv priv
COPY rel rel
COPY lib lib
RUN mix do deps.get, deps.compile

WORKDIR /githook/assets
RUN apk add --update --no-cache --virtual .gyp g++ make python3 && \
    npm install && \
    npm run deploy


WORKDIR /githook
RUN mix phx.digest

RUN mix release githook

# Run Release
FROM alpine:3.12

RUN apk add --update --no-cache bash git ncurses-libs openssl
    # Phoenix needs these

WORKDIR /app

RUN chown nobody:nobody /app
USER nobody:nobody

COPY --from=builder --chown=nobody:nobody /githook/dist/ ./

ENV MIX_ENV prod
ENV HOME=/app

CMD ["bin/githook", "start"]
