[![documentation](https://img.shields.io/badge/docs-pages-blue.svg)](https://ScottTheMeme.gitlab.io/githook)
[![pipeline status](https://gitlab.com/ScottTheMeme/githook/badges/master/pipeline.svg)](https://gitlab.com/ScottTheMeme/githook/-/commits/master)
[![coverage report](https://gitlab.com/ScottTheMeme/githook/badges/master/coverage.svg)](https://gitlab.com/ScottTheMeme/githook/-/commits/master)
[![version](https://img.shields.io/badge/version-0.0.1-orange.svg)](https://gitlab.com/ScottTheMeme/githook/-/commits/master)

# This project has no affiliation with GitLab

# Githook

Gitlab webhook processor

## Table of Contents

- [Project Dependencies](#project-dependencies)
- [Getting Started](#getting-started)

## Project Dependencies

This project requires these dependencies to be installed and running:

- Elixir 1.12.x
- Erlang 24.x
- Postgres 12.x

## Getting Started

1. Clone the repository.

```shell
git clone git@gitlab.com:ScottTheMeme/githook.git && cd githook
```

2. Setup the project.
   runs `deps.get`, `ecto.setup`, and `npm i` (in the assets folder)

```shell
mix setup
```

3. Run the project.

```shell
iex -S mix
```

## Contributing

### Testing

Unit tests can be run with `mix test` or `mix coveralls.html`.

### Formatting

This project uses Elixir's `mix format` for formatting. Add a hook in your editor of choice to
run it after a save. Be sure it respects this project's `.formatter.exs`.
