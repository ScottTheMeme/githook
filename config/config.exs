# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :githook,
  ecto_repos: [Githook.Repo]

# Configures the endpoint
config :githook, GithookWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "xS5AhsGZZwb/lB1rHNfRDpPiT4JXOL8A4iXGQ2/c7+uKeWG9MaoN5Ssm08ycFwyf",
  render_errors: [view: GithookWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Githook.PubSub,
  live_view: [signing_salt: "XNI4G29K"],
  server: true

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
