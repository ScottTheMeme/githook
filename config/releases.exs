import Config

primary_discord_webhook_url = System.fetch_env!("PRIMARY_DISCORD_WEBHOOK_URL")

config :githook,
  primary_discord_webhook_url: primary_discord_webhook_url

database_url =
  System.get_env("DATABASE_URL") ||
    raise """
    environment variable DATABASE_URL is missing.
    For example: ecto://USER:PASS@HOST/DATABASE
    """

config :githook, Githook.Repo,
  # ssl: true,
  url: database_url,
  pool_size: String.to_integer(System.get_env("POOL_SIZE") || "10")

config :githook, GithookWeb.Endpoint, secret_key_base: System.fetch_env!("API_SECRET_KEY_BASE")
