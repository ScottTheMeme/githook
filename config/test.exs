use Mix.Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :githook, Githook.Repo,
  database: System.get_env("POSTGRES_DB") || "in_stock_scraper_test",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  pool: Ecto.Adapters.SQL.Sandbox,
  username: System.get_env("POSTGRES_USER") || "postgres"

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :githook, GithookWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
