defmodule Githook.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children =
      [
        Githook.Repo,
        GithookWeb.Telemetry,
        {Phoenix.PubSub, name: Githook.PubSub},
        GithookWeb.Endpoint,
        Githook.TypeStore
      ]
      |> Enum.filter(&(not is_nil(&1)))

    opts = [strategy: :one_for_one, name: Githook.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def config_change(changed, _new, removed) do
    GithookWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
