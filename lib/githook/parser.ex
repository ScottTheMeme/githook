defmodule Githook.Parser do
  @moduledoc """
  Parses the incoming events and routes them accordingly.
  """

  alias Githook.TypeStore

  require Logger

  def run(event) do
    event = keys_to_atoms(event)
    module = TypeStore.lookup_event_type(event.object_kind)
    module.run(event)
  end

  def keys_to_atoms(string_key_map) when is_map(string_key_map) do
    for {key, val} <- string_key_map, into: %{}, do: {String.to_atom(key), keys_to_atoms(val)}
  end

  def keys_to_atoms(string_key_map_list) when is_list(string_key_map_list) do
    for val <- string_key_map_list, do: keys_to_atoms(val)
  end

  def keys_to_atoms(value), do: value
end
