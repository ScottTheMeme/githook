defmodule Githook.Repo do
  use Ecto.Repo,
    otp_app: :githook,
    adapter: Ecto.Adapters.Postgres
end
