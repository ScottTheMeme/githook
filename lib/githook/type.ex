defmodule Githook.Type do
  @moduledoc """
  Webhook type setup at application boot for GitLab webhooks.
  """

  alias Githook.{Type, TypeStore}

  @event_kind %{
    "push" => Type.Push,
    "tag_push" => Type.TagPush,
    "issue" => Type.Issue,
    "note" => Type.Note,
    "merge_request" => Type.MergeRequest,
    "wiki_page" => Type.WikiPage,
    "pipeline" => Type.Pipeline,
    "build" => Type.Job,
    "deployment" => Type.Deployment,
    "feature_flag" => Type.FeatureFlag,
    "release" => Type.Release
  }

  def run, do: load_events()

  defp load_events do
    Enum.each(@event_kind, fn {type, module} -> TypeStore.add_event_type([type], module) end)
  end
end
