defmodule Githook.TypeStore do
  @moduledoc """
  ETS table
  """

  use GenServer
  alias Githook.Type

  @default_table :events
  @default_table_options [{:read_concurrency, true}, :ordered_set, :public, :named_table]

  def add_event_type([type], module, table \\ @default_table) do
    case lookup_event_type(type, table) do
      nil ->
        :ets.insert(table, {type, module})
        :ok

      module when not is_map(module) ->
        {:error, "type `#{type}` already exists with the module `#{module}`"}
    end
  end

  def lookup_event_type(event_type, table \\ @default_table) do
    case :ets.lookup(table, event_type) do
      [] -> nil
      [{_name, module}] -> module
    end
  end

  def all_event_types(table \\ @default_table) do
    table
    |> :ets.tab2list()
    |> Enum.reduce(%{}, fn {type, module}, acc -> Map.put(acc, type, module) end)
  end

  def start_link(
        table_name \\ @default_table,
        table_options \\ @default_table_options,
        gen_options
      ) do
    GenServer.start_link(__MODULE__, {table_name, table_options}, gen_options)
  end

  def init({table_name, table_options}) do
    tid = :ets.new(table_name, table_options)
    Type.run()

    {:ok, tid}
  end

  def handle_call(:tid, _, tid) do
    {:reply, tid, tid}
  end
end
