defmodule Githook.WebhookProcessor do
  @moduledoc """
  Processes the webhook post to discord.
  """

  require Logger

  def send(_, body) do
    body = Jason.encode!(body)
    {:ok, url} = Application.fetch_env(:plexhook, :primary_discord_webhook_url)
    Tesla.post(url, body, [{"Content-Type", "application/json"}])
  end
end
