defmodule GithookWeb.PageController do
  use GithookWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
