defmodule GithookWeb.WebhookController do
  use GithookWeb, :controller

  def processor(conn, params) do
    Task.async(fn -> Githook.Parser.run(params) end)

    conn
    |> put_status(200)
    |> json(%{success: true})
  end
end
