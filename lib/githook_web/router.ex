defmodule GithookWeb.Router do
  use GithookWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", GithookWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/webhook", GithookWeb do
    pipe_through :api

    post "/", WebhookController, :processor
  end

  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: GithookWeb.Telemetry
    end
  end
end
